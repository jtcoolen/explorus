Explorus changelog
==================

Version DEV
===========

Added
-----

-  Oxfordnet activation
   `MR!380 <https://gitlab.com/functori/dev/explorus/-/merge_requests/380>`__
-  New computation regarding adaptive issuance and rewards 
   `MR!385 <https://gitlab.com/functori/dev/explorus/-/merge_requests/385>`__

Fixed
-----

Changed
-------

-  Remove all iterations of 'endorsement' in favor of 'attestation' 
   `MR!382 <https://gitlab.com/functori/dev/explorus/-/merge_requests/382>`__

Deprecated
----------

Version 2.0
===========

Added
-----

-  Nairobinet activation
   `MR!354 <https://gitlab.com/functori/dev/explorus/-/merge_requests/354>`__

-  Title on pages
   `MR!350 <https://gitlab.com/functori/dev/explorus/-/merge_requests/350>`__
   `MR!351 <https://gitlab.com/functori/dev/explorus/-/merge_requests/351>`__

-  SORU’s refutation game
   `MR!346 <https://gitlab.com/functori/dev/explorus/-/merge_requests/346>`__

-  SORU’s conflicts
   `MR!345 <https://gitlab.com/functori/dev/explorus/-/merge_requests/345>`__

-  SORU’s stakers info
   `MR!343 <https://gitlab.com/functori/dev/explorus/-/merge_requests/343>`__

-  Origination to operation explorer
   `MR!338 <https://gitlab.com/functori/dev/explorus/-/merge_requests/338>`__

-  Link to all network constants
   `MR!336 <https://gitlab.com/functori/dev/explorus/-/merge_requests/336>`__

-  Documentation
   `MR!335 <https://gitlab.com/functori/dev/explorus/-/merge_requests/335>`__
   `MR!334 <https://gitlab.com/functori/dev/explorus/-/merge_requests/334>`__

-  Soru for mumbainet
   `MR!328 <https://gitlab.com/functori/dev/explorus/-/merge_requests/328>`__
   `MR!329 <https://gitlab.com/functori/dev/explorus/-/merge_requests/329>`__

-  SORU: make the smart rollup ID select bar searchable
   `MR!372 <https://gitlab.com/functori/dev/explorus/-/merge_requests/372>`__

-  CHANGELOGS pages for explorus and tezos repository
   `MR!374 <https://gitlab.com/functori/dev/explorus/-/merge_requests/374>`__

Fixed
-----

-  SORU’s commitment hover erasing (Chrome)
   `commit!572ba1 <https://gitlab.com/functori/dev/explorus/-/commit/572ba167a2f510d77d1a57909cad1283b610fa81>`__

-  SORU’s general board odd overflow’s fix (Chrome)
   `commit!d23792 <https://gitlab.com/functori/dev/explorus/-/commit/d23792ccfb730c4ebef679af8f7c0a88e4f6fa3e>`__

-  Unavailable network constants
   `MR!347 <https://gitlab.com/functori/dev/explorus/-/merge_requests/347>`__

-  Better baking rights computation
   `MR!341 <https://gitlab.com/functori/dev/explorus/-/merge_requests/341>`__

-  Clock computation
   `MR!333 <https://gitlab.com/functori/dev/explorus/-/merge_requests/333>`__

-  Stress_potential_persistent_address test
   `MR!327 <https://gitlab.com/functori/dev/explorus/-/merge_requests/327>`__

-  Handle bakers activity on genesis block
   `MR!326 <https://gitlab.com/functori/dev/explorus/-/merge_requests/326>`__

-  Fix network constants reward encoding
   `MR!371 <https://gitlab.com/functori/dev/explorus/-/merge_requests/371>`__

-  Fix participation encoding
   `MR!376 <https://gitlab.com/functori/dev/explorus/-/merge_requests/376>`__

Changed
-------

-  Rename purely interface related iteration of endorsement to
   attestation
   `MR!355 <https://gitlab.com/functori/dev/explorus/-/merge_requests/355>`__
   `MR!373 <https://gitlab.com/functori/dev/explorus/-/merge_requests/373>`__

-  Improve the overall display of the SORU page
   `MR!344 <https://gitlab.com/functori/dev/explorus/-/merge_requests/344>`__

-  Improve jsoo data structure usage
   `MR!342 <https://gitlab.com/functori/dev/explorus/-/merge_requests/342>`__

-  Ask the user to lauch the commitment_tree
   `MR!339 <https://gitlab.com/functori/dev/explorus/-/merge_requests/339>`__

-  Activate features by protocol and not network
   `MR!331 <https://gitlab.com/functori/dev/explorus/-/merge_requests/331>`__

Deprecated
----------

-  Mumbainet depreciation
   `MR!354 <https://gitlab.com/functori/dev/explorus/-/merge_requests/354>`__

-  Limanet depreciation
   `MR!352 <https://gitlab.com/functori/dev/explorus/-/merge_requests/352>`__

#!/bin/sh

if ! [ $# -eq 0 ]; then
    CSS_FILE=$1
else
    CSS_FILE="static/css/minimal.css"
fi

if ! [ -f "$CSS_FILE" ] ; then
    echo "Error, can not find file : $CSS_FILE"
    exit 1
fi

TIMESTAMP=$(date +"%T")
CSS_CLASS="\.[a-z-]*"
PATTERN_FILE="pattern.track_css"
CSS_TO_REMOVE_FILE="remove.track_css"

clean_up (){
    if [ -f "$PATTERN_FILE" ]; then 
        rm "$PATTERN_FILE"
    fi
}

trap clean_up EXIT

grep -oh -Rw $CSS_FILE -e "$CSS_CLASS" > "$PATTERN_FILE"

echo "TS : $TIMESTAMP" >> "$CSS_TO_REMOVE_FILE"

while IFS= read -r pattern;  
do
    if ! (git grep -q "$pattern" ":!$CSS_FILE"); then 
        echo "\e[31m$pattern \e[32mis not used.\e[0m"
        echo "$pattern" >> "$CSS_TO_REMOVE_FILE"
    fi
done <"$PATTERN_FILE"

rm "$PATTERN_FILE"

no_dead_css_file_ending="TS :"
nb_char_file_ending="${#no_dead_css_file_ending}"
check_if_file_ending_has_no_dead_css=$(tail -1 $CSS_TO_REMOVE_FILE | head -c $nb_char_file_ending)

if [ "$check_if_file_ending_has_no_dead_css" = "$no_dead_css_file_ending" ] ; then 
    echo "Success: there is no dead css"
    exit 0
else
    echo "Failure: there is still some dead css."
    exit 1
fi 

# SRC

This folder contains the main code base of explorus. It is split in 3 folders:

-   `common` contains all the files that are **JavaScript independant** and that are making the
    construction of any explorus components easier.
-   `lib` contains a collection of useful libraries that can be used throughout all the code.
-   `vue` contains all the vue components that compose and orchestrate the final explorus' app.

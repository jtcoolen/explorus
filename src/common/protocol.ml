(*****************************************************************************)
(*                                                                           *)
(* MIT License                                                               *)
(*                                                                           *)
(* Copyright (c) 2023, Functori <contact@functori.com>                       *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included in*)
(* all copies or substantial portions of the Software.                       *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGESOR OTHER *)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR INCONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)

(** [ALPHA] is the default protocol that will predominate over other protocols
    since it's the only protocol that will never be removed, and is the only
    evolving one. *)
type t =
  | ALPHA
  | MUMBAI

let default = ALPHA

let latest_release = function
  | ALPHA -> 17
  | MUMBAI -> 16

let compare protoA protoB =
  compare (latest_release protoA) (latest_release protoB)

let identification protocol =
  let identify pattern = String.exists protocol pattern in
  if identify "Mumbai" then
    MUMBAI
  else
    ALPHA

module Infix = struct
  let ( < ) x y = compare x y < 0

  let ( <= ) x y = compare x y <= 0

  let ( > ) x y = compare x y > 0

  let ( >= ) x y = compare x y >= 0
end
